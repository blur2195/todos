import React, { useEffect, useState } from "react";
import Footer from "./components/Footer";
import TodoInput from "./components/TodoInput";
import TodoItem from "./components/TodoItem";
import { ACTIVE_TODOS, ALL_TODOS, COMPLETED_TODOS } from "./helper/constants";
import { generateId } from "./helper/utils";

const App = () => {
  const [todos, setTodos] = useState([]);
  const [editing, setEditing] = useState(null);
  const [completedAll, setCompletedAll] = useState(false);
  const [status, setStatus] = useState(ALL_TODOS);

  const addTodo = (val) => {
    setTodos(todos.concat({
      id: generateId(),
      content: val,
      completed: false,
    }));
  };

  const toggleAll = () => {
    setTodos(todos.map(todo => ({
      ...todo,
      completed: !completedAll,
    })));
    setCompletedAll(!completedAll);
  }

  const toggleTodo = (id) => {
    setTodos(todos.map(todo => ({
      ...todo,
      completed: todo.id === id ? !todo.completed : todo.completed,
    })));
  };

  const saveTodo = (id, val) => {
    setTodos(todos.map(todo => ({
      ...todo,
      content: todo.id === id ? val : todo.content,
    })));
    setEditing(null);
  };

  const deleteTodo = (id) => {
    setTodos(todos.filter(todo => todo.id !== id));
  };

  const clearCompleted = () => {
    setTodos(todos.filter(todo => !todo.completed));
  }

  const remainTodos = todos.filter(todo => !todo.completed).length;
  const completedTodos = todos.length - remainTodos;

  const shownTodos = todos.filter((todo) => {
    switch (status) {
      case ACTIVE_TODOS:
        return !todo.completed;
      case COMPLETED_TODOS:
        return todo.completed;
      default:
        return true;
    }
  });

  useEffect(() => {
    if (!!todos.length) {
      setCompletedAll(remainTodos === 0);
    }
  }, [remainTodos]);

  return (
    <section className="todoapp">
      <TodoInput
        addTodo={addTodo}
        toggleAll={toggleAll}
        show={!!todos.length}
        checked={!!todos.length && remainTodos === 0}
      />
      {
        shownTodos.map(todo => (
          <TodoItem
            key={todo.id}
            todo={todo}
            editing={editing === todo.id}
            toggleTodo={toggleTodo}
            onEdit={() => setEditing(todo.id)}
            saveTodo={(val) => saveTodo(todo.id, val)}
            onCancel={() => setEditing(null)}
            deleteTodo={() => deleteTodo(todo.id)}
          />
        ))
      }
      {
        !!todos.length && (
          <Footer
            remain={remainTodos}
            completed={completedTodos}
            status={status}
            setStatus={setStatus}
            clearCompleted={clearCompleted}
          />
        )
      }
    </section>
  );
};

export default App;
