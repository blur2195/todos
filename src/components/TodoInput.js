import React, { useEffect, useRef } from "react";
import { ENTER_KEY } from "../helper/constants";

const TodoInput = ({ addTodo, toggleAll, show, checked }) => {
  const inputRef = useRef(null);
  const checkboxRef = useRef(null);

  const handleKeyDown = (e) => {
    if (e.keyCode !== ENTER_KEY) {
      return;
    }
    e.preventDefault();

    const val = inputRef.current.value.trim();
    if (val) {
      addTodo(val);
      inputRef.current.value = '';
    }
  };

  const handleChange = (e) => {
    const el = checkboxRef.current;
    if (el) {
      toggleAll();
      el.checked = e.target.value;
    }
  }

  useEffect(() => {
    const el = checkboxRef.current;
    if (el) {
      el.checked = checked;
    }
  }, [checked]);

  return (
    <div className="todo-input-container">
      <input ref={checkboxRef} id="toggle-all" type="checkbox" className="toggle-all" onChange={handleChange} />
      {show && <label htmlFor="toggle-all"></label>}
      <input
        ref={inputRef}
        placeholder="What needs to be done?"
        autoFocus
        onKeyDown={handleKeyDown}
      />
    </div>
  );
};

export default TodoInput;