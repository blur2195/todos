import React, { useEffect, useMemo, useRef, useState } from "react";
import classNames from "classnames";
import { ENTER_KEY, ESCAPE_KEY } from "../helper/constants";

const TodoItem = (props) => {
  const todo = useMemo(() => props.todo, [props.todo]);
  const [editText, setEditText] = useState(todo.content);

  const inputRef = useRef(null);
  const checkboxRef = useRef(null);

  const handleEdit = () => {
    props.onEdit();
    setEditText(todo.content);
  }

  const handleKeyDown = (e) => {
    if (e.keyCode === ESCAPE_KEY) {
      setEditText(todo.content);
      props.onCancel();
    } else if (e.keyCode === ENTER_KEY) {
      handleSubmit(e);
    }
  }

  const handleChange = (e) => {
    setEditText(e.target.value);
  }

  const handleSubmit = (e) => {
    const val = editText.trim();
    if (val) {
      props.saveTodo(val);
      setEditText(val);
    } else {
      props.deleteTodo();
    }
  }

  useEffect(() => {
    const el = inputRef.current;
    if (props.editing && el) {
      el.focus();
      el.setSelectionRange(el.value.length, el.value.length);
    }
  }, [props.editing]);

  useEffect(() => {
    const el = checkboxRef.current;
    if (el) {
      el.checked = todo.completed;
    }
  }, [todo.completed]);

  return (
    <div className={classNames(
      "todo-item",
      {
        completed: todo.completed,
        editing: props.editing,
      }
    )}>
      <input ref={checkboxRef} className="toggle" type="checkbox" onChange={() => props.toggleTodo(todo.id)} />
      <span className="content" onDoubleClick={() => handleEdit()}>{editText}</span>
      <button className="destroy" onClick={() => props.deleteTodo()}></button>
      <input
        ref={inputRef}
        className="edit-input"
        value={editText}
        onKeyDown={handleKeyDown}
        onChange={handleChange}
        onBlur={e => handleSubmit(e)}
      />
    </div>
  );
};

export default TodoItem;