import React from "react";
import classNames from "classnames";
import { ACTIVE_TODOS, ALL_TODOS, COMPLETED_TODOS } from "../helper/constants";

const Footer = ({ remain, completed, status, setStatus, clearCompleted }) => {
  const pluralize = (count, word) => {
    return count === 1 ? word : word + 's';
  }

  return (
    <div className="footer">
      <span>{`${remain} ${pluralize(remain, 'item')} left`}</span>
      <ul className="filter">
        { [ALL_TODOS, ACTIVE_TODOS, COMPLETED_TODOS].map((s, i) => (
          <li
            key={i}
            className={classNames({
              active: status === s,
            })}
            onClick={() => setStatus(s)}
          >{s}</li>
        )) }
      </ul>
      <div className="clear-completed">
        {
          completed > 0 && <button
            onClick={() => clearCompleted()}
          >Clear completed</button>
        }
      </div>
    </div>
  );
};

export default Footer;